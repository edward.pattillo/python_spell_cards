import sqlite3
import os
import csv

# open database connection
def db_open():

    try:
        connection = sqlite3.connect('database.db')

        # this line allows us to retreive results as a python dictionary
        connection.row_factory = sqlite3.Row

        cursor = connection.cursor()
        # print("Successfully Connected to SQLite")
        return cursor,connection

    except sqlite3.Error as error:
        print("Failed to connect to sqlite table", error)
        exit


#close database connection
def db_close(cursor,connection):

    cursor.close()
    connection.close()


# check if a value exists in a table
def check_if_exists(table,column,value):

    # this escapes any single quotes so it doesn't mess up the SQL statement
    value = value.replace("'","''")

    sql = "SELECT EXISTS(SELECT 1 FROM "+table+" WHERE "+column+"='"+value+"')"
    # print(sql)
    cursor,connection = db_open()
    cursor.execute(sql)
    
    result = cursor.fetchone()

    if not result[0]:
        exists = False
    else:
        exists = True
            
    db_close(cursor,connection)
    return exists


# get id for matching foreign keys
def get_id(table,column,value,column_needed):

    # this escapes any single quotes so it doesn't mess up the SQL statement
    value = value.replace("'","''")

    sql = "SELECT * FROM "+table+" WHERE "+column+"='"+value+"';"

    cursor,connection = db_open()
    cursor.execute(sql)
    result = cursor.fetchone()
    db_close(cursor,connection)
    return int(result[column_needed])




# generic db insert function
def insert_stuff(table,columns,values):
    
    if len(columns) > 1:
        column_string = ','
    else:
        column_string = ''

    column_string = column_string.join(columns)

    qString = ''

    # make '?'s for prepared statements
    for i in range(1,len(values)+1):

        qString += '?'

        if i != len(values):

            qString += ','

    sql = "INSERT INTO "+table+" ("+column_string+") values ("+qString+");"
    # print("sql:",sql)

    cursor,connection = db_open()

    cursor.execute(sql,values)
    connection.commit()

    db_close(cursor,connection)

    return True




def process_spell(this_spell):

    #  ------------  start with classes ----------------- 
    class_name = this_spell['class']
    # clean up class names
    if "(" in class_name:
        # print(class_name,end=" changed to ")
        class_name = class_name.split("(",1)[0]
        # print(class_name)
    
    # strip whitespace from the ends of the class names
    this_spell['class'] = class_name.strip()

    # if the class name isn't already in the database
    if not check_if_exists("classes","class_name",this_spell['class']):
        
        print(this_spell['class'],"isn't in the 'classes' table in the db.")
        # insert new class name into database
        insert_stuff("classes",["class_name"],[this_spell['class']])

    # --------------- get class id -----------------

    class_id = get_id('classes','class_name',this_spell['class'],'id_classes')
    # don't need this any more
    del this_spell['class']

  


    # ---- next make sure spell doesn't already exist in database ----
    this_spell['spell_name'] = this_spell['spell_name'].strip()

    # if the spell name is already in the database
    if check_if_exists("spells", "spell_name", this_spell['spell_name']):

        # link class to existing spell
        spell_id = get_id('spells','spell_name',this_spell['spell_name'],'id_spells')
        insert_stuff("spells_classes",["id_spells","id_classes"],[spell_id,class_id])

        # jump out of function
        return


    # ---------------  deal with schools next ------------------
    school_name = this_spell['school']

    # clean up school name
    if "level " in school_name:
        # print(school_name,end=" changed to ")
        school_name = school_name.split("level ",1)[1]
        # print(school_name)

    if " cantrip" in school_name:
        # print(school_name,end=" changed to ")
        school_name = school_name.split(" cantrip",1)[0]
        # print(school_name)

    # strip whitespace from the ends of the class names
    this_spell['school'] = school_name.strip()

    # if the school name isn't already in the database
    if not check_if_exists("schools","school_name",this_spell['school']):

        print(this_spell['school'],"isn't in the 'schools' table in the db.")
        # insert new school name into database
        insert_stuff("schools",["school_name"],[this_spell['school']])


    # ---------------  deal with components next ------------------
    components = this_spell['components'].strip()

    # don't need this any more as we have seperate fields for components in our data stucture
    del this_spell['components']

    this_spell['spell_verbal'] = 0
    this_spell['spell_somatic'] = 0
    this_spell['spell_material'] = 0

    if "V" in components:
        this_spell['spell_verbal'] = 1
    if "S" in components:
        this_spell['spell_somatic'] = 1
    if "M" in components:
        this_spell['spell_material'] = 1

        # if the description contains the spell materials at the beginning in ()
        if this_spell['spell_description'].strip()[0] == '(':
            # pull spell_materials_required from the beginning of spell_description
            desc_list = this_spell['spell_description'].split(")",1)
            this_spell['spell_materials_required'] = desc_list[0].replace("(", "").capitalize()

            # rewrite spell_description without materials in ()
            this_spell['spell_description'] = desc_list[1]

            # print('---------------------------------------')
            # print(this_spell['spell_materials_required'])
            # print(this_spell['spell_description'])



    # ---------------  deal with spell_level next ------------------
    this_spell['spell_level'] = int(this_spell['spell_level'].strip())

    # ---------------  deal with spell_casting_time next ------------------
    this_spell['spell_casting_time'] = this_spell['spell_casting_time'].strip()

    # ---------------  deal with spell_range next ------------------
    this_spell['spell_range'] = this_spell['spell_range'].strip()

    # ---------------  deal with spell_casting_time next ------------------
    this_spell['spell_casting_time'] = this_spell['spell_casting_time'].strip()

    # ---------------  deal with spell_duration next ------------------
    this_spell['spell_duration'] = this_spell['spell_duration'].strip()

    # ---------------  deal with spell_description next ------------------
    this_spell['spell_description'] = this_spell['spell_description'].strip().replace("<br>", "").replace("â€¢","--")
    
    # --------------- get school id -----------------

    this_spell['spell_school_id'] = get_id('schools','school_name',this_spell['school'],'id_schools')
    # don't need this any more
    del this_spell['school']

    # --------------- insert spell into database --------------
    
    keylist = []
    valuelist = []

    for key,value in this_spell.items():
        keylist.append(key)
        valuelist.append(str(value))

    # print(keylist,valuelist)
    insert_stuff("spells",keylist,valuelist)

    # link class to this spell
    spell_id = get_id('spells','spell_name',this_spell['spell_name'],'id_spells') 
    insert_stuff("spells_classes",["id_spells","id_classes"],[spell_id,class_id])


# -------------- all procedural code goes under here ................
if __name__ == "__main__":

    con = sqlite3.connect("database.db")

    with open("schema.sql") as schema:
        con.executescript(schema.read())

    # --------------   make a list of CSV files to be parsed -------------------------
    print("Files to be parsed:")
    file_list = []
    directory = 'data_files'
    for entry in os.scandir(directory):
        if (entry.path.endswith(".csv")) and entry.is_file():
            print(entry.path)
            file_list.append(entry.path)
    print()
    # --------------------------------------------------------------------------------

    # -------------- read CSV and prepare each line to be inserted into database -------------------------
    total_lines = 0
    for spell_file in file_list:

        with open(spell_file) as csv_file:

            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0

            for row in csv_reader:

                if line_count == 0:
                    # print("Column names for: "+spell_file+" , ".join(row))
                    line_count += 1
                    total_lines += 1
                else:
                    this_spell = {"class":row[0], "spell_level":row[1], "spell_name":row[2], "school":row[3], "spell_casting_time":row[4], "spell_range":row[5], "components":row[6], "spell_duration":row[7], "spell_description":row[8]}
                    process_spell(this_spell)
                    line_count += 1
                    total_lines += 1
                
            print('Processed',line_count,'spells in this file:',spell_file)

    print("Proccessed",total_lines,"spells in total.")

