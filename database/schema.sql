CREATE TABLE 'spells' (
'id_spells' INTEGER DEFAULT NULL PRIMARY KEY AUTOINCREMENT,
'spell_name' TEXT(35) DEFAULT NULL,
'spell_school_id' TEXT(25) DEFAULT NULL REFERENCES 'schools' ('id_schools'),
'spell_level' INTEGER DEFAULT NULL,
'spell_casting_time' TEXT(25) DEFAULT NULL,
'spell_range' TEXT(25) DEFAULT NULL,
'spell_duration' TEXT(25) DEFAULT NULL,
'spell_verbal' INTEGER(1) DEFAULT NULL,
'spell_somatic' INTEGER(1) DEFAULT NULL,
'spell_material' INTEGER(1) DEFAULT NULL,
'spell_materials_required' TEXT(50) DEFAULT NULL,
'spell_description' TEXT DEFAULT NULL
);

CREATE TABLE 'schools' (
'id_schools' INTEGER DEFAULT NULL PRIMARY KEY AUTOINCREMENT,
'school_name' TEXT(25) DEFAULT NULL
);

CREATE TABLE 'classes' (
'id_classes' INTEGER DEFAULT NULL PRIMARY KEY AUTOINCREMENT,
'class_name' TEXT(25) DEFAULT NULL,
'class_description' TEXT DEFAULT NULL
);

CREATE TABLE 'spells_classes' (
'id_spells_classes' INTEGER DEFAULT NULL PRIMARY KEY AUTOINCREMENT,
'id_spells' INTEGER DEFAULT NULL REFERENCES 'spells' ('id_spells'),
'id_classes' INTEGER DEFAULT NULL REFERENCES 'classes' ('id_classes')
);