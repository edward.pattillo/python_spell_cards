import sqlite3


# open database connection
def db_open():

    try:
        connection = sqlite3.connect('database/database.db')

        # this line allows us to retreive results as a python dictionary
        connection.row_factory = sqlite3.Row

        cursor = connection.cursor()
        print("Successfully Connected to SQLite")
        return cursor,connection

    except sqlite3.Error as error:
        print("Failed to connect to sqlite table", error)
        exit


#close database connection
def db_close(cursor,connection):

    cursor.close()
    connection.close()


# converts a row of results into a dictionary
def dict_from_row(row):
    return dict(zip(row.keys(), row))  


def present_spell(spell):

    print()
    print('--------------------------------------------------------')
    print("| Spell name:", spell['spell_name'])
    print("| Level:", spell['spell_level'])
    print("| Description:", spell['spell_description'])
    print('--------------------------------------------------------')


# -------------- all procedural code goes under here ................
if __name__ == "__main__":


    sql = "SELECT * FROM spells WHERE spell_level = 9"
    # print(sql)
    cursor,connection = db_open()
    
    cursor.execute(sql)
    result = cursor.fetchall()
    db_close(cursor,connection)

    spell_list = []
    for row in result:
        this_dict = dict_from_row(row)
        spell_list.append(this_dict)


    for spell in spell_list:

        present_spell(spell)

    # print(spell_list)
    print(len(spell_list), 'results.')


