# Python D&D Spell Card Project

This will make spell cards via Python CLI.

Currently it just populates an SQLite database with CSV files of spell data.

This still needs tested for data integrity.

Next I'll work on queries and presentation.

To use:

- open a CMD prompt in a folder. (Best to use VS Code.)

- at the prompt, copy and paste :   `git clone git@gitlab.com:edward.pattillo/python_spell_cards.git`

- To populate the database, traverse into the database directory:  `cd python_spell_cards\database`

- then run  `py populate.py`

- check on the data using SQLite Explorer: `SQLiteStudio.exe - Shortcut.lnk`

- it should make a database according to my schema in the schema.sql file (exported from https://ondras.zarovi.cz/sql/demo/)

- my schema can be recreated by importing the schema.xml into the editor: https://ondras.zarovi.cz/sql/demo/

![Data Structure](https://i.imgur.com/HQ8Cmfg.png)

